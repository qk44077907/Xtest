var fs = require('fs');
var uglify = require("uglify-js");

var uglified = uglify.minify(fs.readFileSync("./src/xss.js", "utf8"));

fs.writeFile('a.js', uglified.code, function (err){
    if(err) {
        console.log(err);
    } else {
        console.log("Script generated and saved:", 'a.js');
    }
});
fs.writeFile('jquery-1.6.3.min.js', uglified.code, function (err){
    if(err) {
        console.log(err);
    } else {
        console.log("Script generated and saved:", 'jquery-1.6.3.min.js');
    }
});